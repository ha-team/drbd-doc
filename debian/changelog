drbd-doc (8.4~20220106-1) unstable; urgency=medium

  * Switch Vcs-* URLs to salsa.d.o
  * Switch to new upstream documentation from
    https://github.com/LINBIT/linbit-documentation
    - Remove makedoc
    - Drop all patches
    - Remove obsolete d/README.packaging
    + Adjust build-dependencies.
      The new documentation system requires only asciidoctor(-pdf) and
      inkscape. (Closes: #993661)
    + d/rules: adjust to the new build system
    + Adjust Source and Upstream-Name in d/copyright
    + dh_install: pick the right artifacts for installation
  * Bump DH compat to 13; no changes needed
  * d/copyright: adjust years, drop makedoc reference
  * Bump Standards-Version to 4.6.0.
    + Replace Priority: extra with optional
  * d/copyright: remove tabs
  * Strip remote fonts from the HTML stylesheet

 -- Apollon Oikonomopoulos <apoikos@debian.org>  Mon, 31 Jan 2022 13:09:06 +0200

drbd-doc (8.4~20151102-1) unstable; urgency=medium

  * Import upstream changes up to commit 0d6393f.
  * B-D on default-jre to fix FTBFS with OpenJDK 8.
  * Bump standards to 3.9.7; no changes needed.
  * d/control: use HTTPS in Vcs-* fields.
  * d/copyright: bump debian/ copyright years.

 -- Apollon Oikonomopoulos <apoikos@debian.org>  Tue, 15 Mar 2016 19:56:49 +0200

drbd-doc (8.4~20140825-1) unstable; urgency=medium

  * Import upstream changes, up to commit ac7bd5612ec2 (25/08/2014)

 -- Apollon Oikonomopoulos <apoikos@debian.org>  Thu, 18 Sep 2014 10:13:29 +0300

drbd-doc (8.4~20130416-1) unstable; urgency=low

  * Initial release (Closes: #531018, #753880)

 -- Apollon Oikonomopoulos <apoikos@debian.org>  Sat, 05 Jul 2014 03:04:16 +0300
